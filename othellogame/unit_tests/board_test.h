#pragma once

#include "board.h"

#define BOARD_FAILED 1

struct BoardState
{
    int tileAmount;
    char posYs[64];
    char posXs[64];
    TileType tiles[64];
};

int runBoardTests();