#include <stdlib.h>
#include <stdio.h>

#include "ai_api.h"

#include "gameNode.h"

int max(int a, int b)
{
    return (a > b) ? a : b;
}

int min(int a, int b)
{
    return (a > b) ? b : a;
}


struct ai_inter interface;

struct ai_boardInter boardInter;
struct ai_actionInter actionInter;


void init(struct ai_inter inter)
{
    interface = inter;
    boardInter = inter.boardInter;
    actionInter = inter.actionInter;
}

int minmax(GameNode* node, int depth, TileType forPlayer, TileType aiPlayer)
{
    if(depth <= 0 || node->childrenAmt == 0)
        return node->score;
    int value;
    if(forPlayer == aiPlayer)
    {
        value = -999999999;
        for(int i = 0; i < node->childrenAmt; i++)
        {
            if(node->children[i] == NULL)
                node->children[i] = createNodeFromLink(boardInter, node, i, aiPlayer);
            value = max(value, minmax(node->children[i], depth - 1, (forPlayer+1)%2, aiPlayer));
        }
    }
    else
    {
        value = 999999999;
        for(int i = 0; i < node->childrenAmt; i++)
        {
            if(node->children[i] == NULL)
                node->children[i] = createNodeFromLink(boardInter, node, i, aiPlayer);
            value = min(value, minmax(node->children[i], depth - 1, (forPlayer+1)%2, aiPlayer));
        }
    }
    node->score = value;
    return value;
}

void runTurn(GameState* state, char pId)
{
    GameNode* root = createNodeFromState(boardInter, state, pId);
    minmax(root, 3, pId, pId);
    int value = -999999999;
    int maxInd = -1;
    for(int i = 0; i < root->childrenAmt; i++)
    {
        if(root->children[i]->score > value)
        {
            value = root->children[i]->score;
            maxInd = i;
        }
    }
    Vec2_c target = root->availableMoves[maxInd];

    int valid = actionInter.placeStone(state, target.posY, target.posX);
    valid++;

    freeGameNode(root);
}

void destroy()
{

}
