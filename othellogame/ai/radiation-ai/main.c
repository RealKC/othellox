#include <stdlib.h>
#include <stdio.h>

#include "ai_api.h"

#include "gameNode.h"

int max(int a, int b)
{
    return (a > b) ? a : b;
}

int min(int a, int b)
{
    return (a > b) ? b : a;
}


struct ai_inter interface;

struct ai_boardInter boardInter;
struct ai_actionInter actionInter;


void init(struct ai_inter inter)
{
    interface = inter;
    boardInter = inter.boardInter;
    actionInter = inter.actionInter;
}

int alphaBeta(GameNode* node, int depth, TileType forPlayer, TileType aiPlayer, int alpha, int beta)
{
    if(depth <= 0 || node->childrenAmt == 0)
        return setHeuristicScore(node, aiPlayer);
    int value;
    if(forPlayer == aiPlayer)
    {
        value = -999999999;
        for(int i = 0; i < node->childrenAmt; i++)
        {
            if(node->children[i] == NULL)
                node->children[i] = createNodeFromLink(boardInter, node, i);
            value = max(value, alphaBeta(node->children[i], depth - 1, (forPlayer+1)%2, aiPlayer, alpha, beta));
            alpha = max(alpha, value);
            if(alpha >= beta)
                break;
        }
    }
    else
    {
        value = 999999999;
        for(int i = 0; i < node->childrenAmt; i++)
        {
            if(node->children[i] == NULL)
                node->children[i] = createNodeFromLink(boardInter, node, i);
            value = min(value, alphaBeta(node->children[i], depth - 1, (forPlayer+1)%2, aiPlayer, alpha, beta));
            beta = min(beta, value);
            if(alpha >= beta)
                break;
        }
    }
    node->score = value;
    return value;
}

void runTurn(GameState* state, char pId)
{
    GameNode* root = createNodeFromState(boardInter, state, pId);
    alphaBeta(root, 4, pId, pId, -999999999, 999999999);
    int value = -999999999;
    int maxInd = -1;
    for(int i = 0; i < root->childrenAmt; i++)
    {
        if(root->children[i] == NULL)
            continue;
        if(root->children[i]->score > value)
        {
            value = root->children[i]->score;
            maxInd = i;
        }
    }
    Vec2_c target = root->availableMoves[maxInd];

    int valid = actionInter.placeStone(state, target.posY, target.posX);
    valid++;

    freeGameNode(root);
}

void destroy()
{

}
