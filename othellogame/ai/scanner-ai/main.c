#include <stdlib.h>
#include <stdio.h>

#include "ai_api.h"

#include "gameNode.h"

#define pINF 999999999
#define nINF -999999999

int max(int a, int b)
{
    return (a > b) ? a : b;
}

int min(int a, int b)
{
    return (a > b) ? b : a;
}


struct ai_inter interface;

struct ai_boardInter boardInter;
struct ai_actionInter actionInter;


void init(struct ai_inter inter)
{
    interface = inter;
    boardInter = inter.boardInter;
    actionInter = inter.actionInter;
}

int alphaBeta(GameNode* node, int depth, TileType forPlayer, TileType aiPlayer, int alpha, int beta)
{
    if(depth <= 0 || node->childrenAmt == 0)
        return setHeuristicScore(node, aiPlayer);
    int value;
    if(forPlayer == aiPlayer)
    {
        value = nINF;
        for(int i = 0; i < node->childrenAmt; i++)
        {
            if(node->children[i] == NULL)
                node->children[i] = createNodeFromLink(boardInter, node, i);
            value = max(value, alphaBeta(node->children[i], depth - 1, (forPlayer+1)%2, aiPlayer, alpha, beta));
            alpha = max(alpha, value);
            if(alpha >= beta)
                break;
        }
    }
    else
    {
        value = pINF;
        for(int i = 0; i < node->childrenAmt; i++)
        {
            if(node->children[i] == NULL)
                node->children[i] = createNodeFromLink(boardInter, node, i);
            value = min(value, alphaBeta(node->children[i], depth - 1, (forPlayer+1)%2, aiPlayer, alpha, beta));
            beta = min(beta, value);
            if(alpha >= beta)
                break;
        }
    }
    node->score = value;
    return value;
}

int mtdf(GameNode* root, int guessArbitrary, int depth, TileType forPlayer)
{
    int guess = guessArbitrary;
    int upper = pINF;
    int lower = nINF;
    while(lower < upper)
    {
        int beta = max(guess, lower+1);
        guess = alphaBeta(root, depth, forPlayer, forPlayer, beta-1, beta);
        if(guess < beta)
            upper = guess;
        else
            lower = guess;
    }
    return guess;
}

void runTurn(GameState* state, char pId)
{
    GameNode* root = createNodeFromState(boardInter, state, pId);
    mtdf(root, 31, 5, pId);
    int value = nINF;
    int maxInd = -1;
    for(int i = 0; i < root->childrenAmt; i++)
    {
        if(root->children[i] == NULL)
            continue;
        if(root->children[i]->score > value)
        {
            value = root->children[i]->score;
            maxInd = i;
        }
    }
    Vec2_c target = root->availableMoves[maxInd];

    int valid = actionInter.placeStone(state, target.posY, target.posX);
    valid++;

    freeGameNode(root);
}

void destroy()
{

}
