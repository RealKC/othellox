#pragma once

#include "ai_api.h"

#define pINF 16777216
#define nINF -16777216

struct GameNodeS;

typedef struct GameNodeS
{
    Vec2_c link;
    TileType player;
    TileType board[8][8];

    int childrenAmt;
    Vec2_c availableMoves[64];
    struct GameNodeS** children;

    float value[4];
    float score[2];
} GameNode;

GameNode* createNodeFromState(struct ai_boardInter inter, GameState* state, char pId);
GameNode* createNodeFromLink(struct ai_boardInter inter, GameNode* node, int move);

void freeGameNode(GameNode* node);
void freeGameNodeUpTo(struct ai_boardInter boardInter, GameNode* node, GameState* find, /*@out@*/ GameNode** output);
void freeGameNodeUpToChild(GameNode* node, GameNode* child);

float getHeuristicScore(struct ai_boardInter boardInter, GameNode* node, TileType forPlayer);
