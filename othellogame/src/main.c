/**
 * @file main.c
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Point d'entrée du programme
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "game.h"
#include "interfaces/allInterfaces.h"

static const int TERMINAL_ONLY = 2;
static const int DIRECT_PLAY = 4;
static const int NO_DISPLAY = 8;
static const int NCURSES = 16;
static const int DO_LOG = 32;

#ifdef ARCHLINUX
char aiDirectory[256] = {'/','u','s','r','/','s','h','a','r','e','/','o','t','h','e','l','l','o','x',0};
#else
char aiDirectory[256] = {'.', 0};
#endif

static int mainLoop();

static void help()
{
    printf("Usage : othellox [--help] [-d] [-l] [-t | -n | -c] [--p1 player1-ai] [--p2 player2-ai] [--ai path] [--timeout value]\n");
}

static int checkOne(int value)
{
    return value == 0 || (value && !(value & (value-1)));
}


int main(int argc, char** argv)
{
    int flags = 0;
    char p1[128] = {0};
    char p2[128] = {0};
    timeoutai = 1.0;
    // Traitement des arguments
    for(int i = 1; i < argc; i++)
    {
        // Si l'argument est une option
        if(argv[i][0] == '-')
        {
            if(strlen(argv[i]) == 1)
            {
                help();
                exit(EXIT_FAILURE);
            }
            // Si c'est une option longue
            if(argv[i][1] == '-')
            {
                if(strcmp(argv[i], "--help") == 0)
                {
                    help();
                    exit(EXIT_FAILURE);
                }
                else if(strcmp(argv[i], "--p1") == 0)
                {
                    if(i+1 < argc)
                    {
                        i++;
                        if(strlen(argv[i]) > 250)
                        {
                            fprintf(stderr, "--p1 argument is too long.\n");
                            exit(EXIT_FAILURE);
                        }
                        strcpy(p1, argv[i]);
                    }
                    else
                    {
                        fprintf(stderr, "Expected player file argument after --p1.\n");
                        exit(EXIT_FAILURE);
                    }
                }
                else if(strcmp(argv[i], "--p2") == 0)
                {
                    if(i+1 < argc)
                    {
                        i++;
                        if(strlen(argv[i]) > 250)
                        {
                            fprintf(stderr, "--p2 argument is too long.\n");
                            exit(EXIT_FAILURE);
                        }
                        strcpy(p2, argv[i]);
                    }
                    else
                    {
                        fprintf(stderr, "Expected player file argument after --p2.\n");
                        exit(EXIT_FAILURE);
                    }
                }
                else if(strcmp(argv[i], "--ai") == 0)
                {
                    if(i+1 < argc)
                    {
                        i++;
                        if(strlen(argv[i]) > 250)
                        {
                            fprintf(stderr, "--ai argument is too long.\n");
                            exit(EXIT_FAILURE);
                        }
                        strcpy(aiDirectory, argv[i]);
                    }
                    else
                    {
                        fprintf(stderr, "Expected folder path argument after --ai.\n");
                        exit(EXIT_FAILURE);
                    }
                }
                else if(strcmp(argv[i], "--timeout") == 0)
                {
                    if (i + 1 < argc)
                    {
                        i++;
                        timeoutai = atof(argv[i]);
                        if(timeoutai <= 0.0)
                        {
                            fprintf(stderr, "Not a valid value for timeout\n");
                            exit(EXIT_FAILURE);
                        }
                    }
                    else
                    {
                        fprintf(stderr,"Expected value after --timeout\n");
                        exit(EXIT_FAILURE);
                    }
                }
            }
            // Si c'est une option courte
            else
                for(unsigned int j = 1; j < strlen(argv[i]); j++)
                {
                    if(argv[i][j] == 't')
                    {
                        flags |= TERMINAL_ONLY;
                    }
                    else if(argv[i][j] ==  'd')
                    {
                        flags |= DIRECT_PLAY;
                    }
                    else if(argv[i][j] == 'c')
                    {
                        flags |= NCURSES;
                    }
                    else if(argv[i][j] == 'n')
                    {
                        flags |= NO_DISPLAY;
                    }
                    else if(argv[i][j] == 'l')
                    {
                        flags |= DO_LOG;
                    }
                }
        }
    }

    // Si l'utilisateur veut plusieurs interfaces en même temps
    if(!checkOne(((flags & NO_DISPLAY) | (flags & TERMINAL_ONLY) | (flags & NCURSES))))
    {
        fprintf(stderr,
            "Othellox can only be run in either ncurses/Terminal/No Display.\n");
        exit(EXIT_FAILURE);
    }

    // Le mode no-display/direct-play ne fonctionne pas sans les options longues p1 et p2.
    if((flags & NO_DISPLAY || flags & DIRECT_PLAY) && (!strcmp(p1, "") || !(strcmp(p2, ""))))
    {
        fprintf(stderr, "Two player files are required to launch in No Display mode or Direct Play.\n");
        exit(EXIT_FAILURE);
    }
    
    // Le mode no-display n'a pas de menu. Alors il force le mode direct-play
    if(flags & NO_DISPLAY)
        flags |= DIRECT_PLAY;

    
	
    return mainLoop(flags, p1, p2);
}


int mainLoop(int flags, char* p1, char* p2)
{
    // Booléanisation (valeur 0 ou 1)
    int logGame = !!(flags & DO_LOG);

    // Choix de l'interface graphique à utiliser suivant la valeur de flags.
    struct interface currentInterface;
    int successChoice = 0;
    if(flags & TERMINAL_ONLY)
    {
        currentInterface = terminalInterface;
        successChoice = 1;
    }
    else if(flags & NO_DISPLAY)
    {
        currentInterface = noDisplayInterface;
        successChoice = 1;
    }
    else if(flags & NCURSES)
    {
#ifndef NO_CURSES
        currentInterface = curInterface;
        successChoice = 1;
#else
        fprintf(stderr, "This build does not support Curses Mode. Default interface chosen.\n");
#endif
    }

    // Si l'utilisateur n'a demandé aucune interface, on utilise une interface par défaut.
    if(!successChoice)
    {
#ifndef NO_NCURSES
        currentInterface = curInterface;
#else
        currentInterface = terminalInterface;
#endif
    }


    if(!currentInterface.init())
    {
        fprintf(stderr, "Impossible to start current interface. Exiting.\n");
        return EXIT_FAILURE;
    }

    // Valeur représentant l'état de l'automate d'exécution du programme.
    ProgramState pState = (flags & DIRECT_PLAY) ? GAME_START : MENU;
    GameState* gState = NULL;

    char* player1 = p1;
    char* player2 = p2;

    // Éxecution de l'automate.
    while(pState != EXIT)
    {
        switch(pState)
        {
            case MENU:
                currentInterface.updateMenu(&pState, &logGame, &player1, &player2);
                currentInterface.drawMenu(pState);
                break;
            case GAME_START:
                gState = initGame(&pState, logGame, player1, player2);
                if(pState == MENU && gState != NULL)
                {
                    free(gState);
                    gState = NULL;
                }
                if(gState == NULL)
                    pState = MENU;
                break;
            case GAME:
                updateGame(&pState, gState, &currentInterface);
                // Si le jeu doit donner une information au joueur (état PLAYER1/PLAYER2/GAMEOVER)
                if(getStatus(gState) == PLAYER1 || getStatus(gState) == PLAYER2 || 
                        getStatus(gState) == GAMEOVER)
                    drawGame(gState, &currentInterface);
                break;
            case GAME_EXIT:
                destroyGameState(gState);
                gState = NULL;
                pState = MENU;
                break;
            case EXITING:
                pState = EXIT;
                // Désallocation des structures créées par l'interface graphique.
                currentInterface.destroy();
                break;
            case EXIT:
                break;
        }
    }

    return EXIT_SUCCESS;
}