/**
 * @file board.c
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Fonctions de gestion du plateau de jeu.
 */

#include "board.h"

#include <stdio.h>
#include "gameState.h"


const Neighbor EMPTY_NEIGHBOR = (Neighbor){-1, -1, -1, -1};


void getNeighbors(Neighbor neighbors[], char posY, char posX)
{
    int i = 0;

    for(char relY = -1; relY < 2; relY++)
    for(char relX = -1; relX < 2; relX++)
    {
        if (relX == 0 && relY == 0)
            continue;
        if (posY+relY >=0 && posX + relX >= 0 && posY+relY < 8 && posX+relX < 8)
            neighbors[i++] = (Neighbor){posY+relY, posX+relX, relY, relX};
    }
    while (i < 8)
        neighbors[i++] = EMPTY_NEIGHBOR;
        
}

/* Dans ce contexte, un voisin valide d'une case veut dire :
*   un voisin qui sera retourné si une pierre est placée sur la case.
*/
void getValidNeighbors(TileType board[8][8], TileType currentPlayer, 
    Neighbor* neighbors, char posY, char posX)
{
    char valid;
    char curY;
    char curX;
    TileType opponent = (currentPlayer+1)%2;

    getNeighbors(neighbors, posY, posX);
    for(int i = 0; i < 8; i++)
        // Si le voisin existe
        if (neighbors[i].posY != -1)
        {
            // On suppose qu'il n'est pas valide
            valid = 0;
            if (board[neighbors[i].posY][neighbors[i].posX] == opponent)
            {
                curY = neighbors[i].posY;
                curX = neighbors[i].posX;
                while (board[curY][curX] == opponent)
                {
                    curY += neighbors[i].relY;
                    curX += neighbors[i].relX;
                    // Si on est en dehors du plateau, on arrête.
                    if (curX < 0 || curX > 7 || curY < 0 || curY > 7)
                        break;
                    // On a réussi à "sandwicher" des pions ennemis. Le voisin est valide.
                    if (board[curY][curX] == currentPlayer)
                        valid = 1;
                }
            }
            if (valid == 0)
            {
                neighbors[i] = EMPTY_NEIGHBOR;
            }
        }
}

char prepareNextTurn(GameState* state)
{
    // On interchange le plateau "board" avec le plateau "futureBoard".
    for(int i = 0; i < 64; i++)
        setTile(state, i/8, i%8, getFutureTile(state, i/8, i%8));
    
    resetAvailableTiles(state);
    
    TileType targetPlayer = T_EMPTY;
    if(getFutureAction(state).player == T_P1)
    {
        setStatus(state, PLAYER2);
        setFutureAction(state, -1, -1, T_P2);
        targetPlayer = T_P2;
    }
    else
    {
        setStatus(state, PLAYER1);
        setFutureAction(state, -1, -1, T_P1);
        targetPlayer = T_P1;
    }

    Neighbor neighbors[8];
    int availNum = 0;
    Vec2_c availMove[64] = {{0,0}};

    // On prépare les cases "jouables".
    for(int y = 0; y < 8; y++)
    for(int x = 0; x < 8; x++)
        if(getTile(state, y, x) == T_EMPTY)
        {
            TileType board[8][8];
            getBoard(state, board);
            getValidNeighbors(board, targetPlayer, neighbors, y, x);
            for(int i = 0; i < 8; i++)
                if(neighbors[i].posX != -1)
                {
                    availMove[availNum++] = (Vec2_c){y, x};
                    break;
                }
        }
    setAvailableTile(state, availMove, availNum);
    
    return availNum > 0;
}

/* Cette variable est volatile, 
*   ce qui permet de l'utiliser de façon sécurisée à travers des threads.
*/
volatile int stableStone = 0;

int placeStone(GameState* state, char posY, char posX)
{
    // La fonction est en cours, l'état du jeu est instable.
    stableStone = 0;

    // On suppose que le coup n'est pas valide.
    char valid = 0;
    for(int i = 0; i < getAvailableTileSize(state); i++)
    {
        Vec2_c tile = getAvailableTile(state, i);
        if(tile.posX == posX && tile.posY == posY)
        {
            // Le coup est valide.
            valid = 1;
            break;
        }
    }
    if(!valid)
    {
        return 0;
    }

    // Si un coup a déjà été joué
    if(getFutureAction(state).posX != -1)
        return 0;
    
    TileType currentPlayer = -1;
    TileType opponent = -1;
    switch(getStatus(state))
    {
        case PLAYER1:
            currentPlayer = T_P1;
            opponent = T_P2;
            break;
        case PLAYER2:
            currentPlayer = T_P2;
            opponent = T_P1;
            break;
        default:
            break;
    }

    // On réalise le coup et retourne les pierres dans le board "futureBoard".
    Neighbor neighbors[8];
    TileType board[8][8];
    getBoard(state, board);
    getValidNeighbors(board, currentPlayer, neighbors, posY, posX);
    
    setFutureTile(state, posY, posX, currentPlayer);
    setFutureAction(state, posY, posX, currentPlayer);
    for(int i = 0; i < 8; i++)
    {
        if(neighbors[i].posX == -1)
            continue;
        char curX = neighbors[i].posX;
        char curY = neighbors[i].posY;
        
        while(getTile(state, curY, curX) == opponent)
        {
            setFutureTile(state, curY, curX, currentPlayer);
            curX += neighbors[i].relX;
            curY += neighbors[i].relY;
        }
    }

    // L'état du jeu est stable à nouveau.
    stableStone = 1;
    return 1;
}
