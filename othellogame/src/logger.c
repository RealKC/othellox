/**
 * @file logger.c
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Fonctions d'aide à l'enregistrement fichier des parties.
 */

#include "logger.h"

#include <time.h>
#include <string.h>
#include <stdlib.h>


FILE* createLogFile()
{
    return createLogFileName("gamelog", 1);
}

FILE* createLogFileName(const char* name, int addTimestamp)
{
    char* fileName = NULL;
    if(addTimestamp)
    {
        time_t timestamp;
        time(&timestamp);
        // 20 est la taille d'un entier 64 bits. (log10(2^64)).
        fileName = (char*)calloc(strlen(name)+20+2, sizeof(char));
        if(fileName == NULL)
            return NULL;
        snprintf(fileName, 256, "%s-%ld", name, timestamp);
    }
    else
    {
        fileName = calloc(strlen(name)+1, sizeof(char));
        if(fileName == NULL)
            return NULL;
        strcpy(fileName, name);
    }
    FILE* logFile = fopen(fileName, "w");
    free(fileName);
    return logFile;
}

void logStart(FILE* logFile, char* p1, char* p2)
{
    if(logFile == NULL)
        return;
    fprintf(logFile, "Player 1: %s\n\tvs\nPlayer 2: %s\n", p1, p2);
}

void logTurn(FILE* logFile, GameAction action)
{
    if(logFile == NULL)
        return;
    fprintf(logFile, "%d : %d %d\n", (int)action.player, (int)action.posX, (int)action.posY);
    fflush(logFile);
}

void logGameOver(FILE* logFile, char* p1, int score1, char* p2, int score2)
{
    if(logFile == NULL)
        return;
    fprintf(logFile, "\nScore : %d - %d\n", score1, score2);
    if(score1 == score2)
        fprintf(logFile, "Tie.");
    else
        fprintf(logFile, "Player %d: %s wins.", 
            (score1 > score2) ? 1 : 2, (score1 > score2) ? p1 : p2);
}

void logFailure(FILE* logFile, char* player, char id)
{
    if(logFile == NULL)
        return;
    fprintf(logFile, "\nCritical Failure of AI %d : %s.\n\tPlayer %d wins by forfeit.", id+1, player, (id+1)%2+1);
}

void logTimeout(FILE* logFile, char* player, char id)
{
    if(logFile == NULL)
        return;
    fprintf(logFile, "\nTimeout of AI %d : %s.\n\tPlayer %d wins by forfeit.", id+1, player, (id+1)%2+1);
}