/**
 * @file curInterface.c
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Interface Ncurses
 */


#include "interfaces/curInterface.h"

#ifndef NO_CURSES
#include <locale.h>
#include <ncurses.h>
#include <string.h>
#include <stdlib.h>

#define BOARD_HEIGHT 19
#define BOARD_WIDTH 39


#ifndef WINDOWS
#include <unistd.h>
#include <dirent.h>
#endif

static int initCur();
static void updateMenuCur(ProgramState* state, int* logGame, char** p1, char** p2);
static void drawMenuCur(ProgramState state);

static PlayerAction getGameInputCur(GameState* gState);
static void drawGameCur(GameState* gState);
static void destroyCur();

char p1Text[256] = {0};
char p2Text[256] = {0};


struct interface curInterface = 
{
    initCur,
    updateMenuCur,
    drawMenuCur,
    getGameInputCur,
    drawGameCur,
    destroyCur
};

static void getScore(GameState* state, int* s1, int* s2)
{
    *s1 = 0, *s2 = 0;
    for(int i = 0; i<64; i++)
        if (getTile(state, i/8, i%8) != T_EMPTY)
        {
            if(getTile(state, i/8, i%8) == T_P1)
                (*s1)++;
            else
                (*s2)++;
        }
}

static int initCur()
{
    setlocale(LC_ALL, "");
    initscr();
    noecho();
    keypad(stdscr, TRUE);
    cbreak();

    if(COLS <= 36) // 36 : Board size in columns
    {
        fprintf(stderr, "Terminal size too small (width) for ncurses display.\n");
        return 0;
    }
    if(LINES <= 20) // 20 : Board size in lines
    {
        fprintf(stderr, "Terminal size too small (height) for ncurses display.\n");
        return 0;
    }
    

    
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(4, COLOR_RED, COLOR_WHITE);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(5, COLOR_BLACK, COLOR_WHITE);
    init_pair(3, COLOR_WHITE, COLOR_BLACK);
    init_pair(6, COLOR_BLACK, COLOR_WHITE);
    
    return 1;
}

enum MenuState
{
    SELECT,
    OPTIONS,
    STARTING,
    EXITED
};

static void handleMenuSelect(int* pastChoice, int* status)
{
    int in = 0;
    int menuChoice = *pastChoice;

    clear();

    attron(COLOR_PAIR(3));
    mvwprintw(stdscr, LINES/2-2, COLS/2 - 4, "OTHELLOX");

    attron(COLOR_PAIR(2 + (menuChoice == 0 ? 3 : 0)));
    mvwprintw(stdscr, LINES/2, COLS/2 - 4, "(S)tart");

    attron(COLOR_PAIR(3 + (menuChoice == 1 ? 3 : 0)));
    mvwprintw(stdscr, LINES/2+1, COLS/2 - 4, "(O)ptions");

    attron(COLOR_PAIR(1 + (menuChoice == 2 ? 3 : 0)));
    mvwprintw(stdscr, LINES/2+2, COLS/2 - 4, "(Q)uit");

    attron(COLOR_PAIR(3));
    mvwprintw(stdscr, LINES/2+4, COLS/2 - 45/2, "Made by Rogalle Quentin and Peyrille Benjamin");
    mvwprintw(stdscr, LINES/2+5, COLS/2 - 58/2, "Project available on https://framagit.org/Arkhist/othellox");

    refresh();
    in = wgetch(stdscr);
    switch(in)
    {
        case KEY_UP:
            *pastChoice = *pastChoice - 1;
            if(*pastChoice < 0)
                *pastChoice = 2;
            break;
        case KEY_DOWN:
            *pastChoice = *pastChoice + 1;
            if(*pastChoice > 2)
                *pastChoice = 0;
            break;
        case 10:
            if(menuChoice == 0)
                *status = STARTING;
            else if(menuChoice == 1)
                *status = OPTIONS;
            else if(menuChoice == 2)
                *status = EXITED;
            *pastChoice = 0;
            break;
        case 's':
        case 'S':
            *status = STARTING;
            *pastChoice = 0;
            break;
        case 'q':
        case 'Q':
            *status = EXITED;
            break;
        case 'o':
        case 'O':
            *status = OPTIONS;
            *pastChoice = 0;
            break;
        default:
            break;
    }
}

static void handleOptions(int* pastChoice, int* status, int* logGame)
{
    int in = 0;
    clear();

    attron(COLOR_PAIR(3));
    mvwprintw(stdscr, LINES/2-2, COLS/2 - 3, "Options");

    attron(COLOR_PAIR(3));
    mvwprintw(stdscr, LINES/2, COLS/2 - 24/2, "Save game logs to a file");

    if(!(*logGame))
    {
        mvwprintw(stdscr, LINES/2+1, COLS/2 - 6, "[");
        mvwprintw(stdscr, LINES/2+1, COLS/2 - 3, "]");
    }
    else
    {
        mvwprintw(stdscr, LINES/2+1, COLS/2 + 3, "[");
        mvwprintw(stdscr, LINES/2+1, COLS/2 + 7, "]");
    }

    attron(COLOR_PAIR(1 + (*pastChoice == 0 ? 3 : 0)));
    mvwprintw(stdscr, LINES/2+1, COLS/2 - 5, "No");

    attron(COLOR_PAIR(2 + (*pastChoice == 1 ? 3 : 0)));
    mvwprintw(stdscr, LINES/2+1, COLS/2 + 4, "Yes");

    attron(COLOR_PAIR(3 + (*pastChoice == 2 ? 3 : 0)));
    mvwprintw(stdscr, LINES/2+3, COLS/2 - 13, "Confirm and Return to menu");

    refresh();
    in = wgetch(stdscr);
    switch(in)
    {
        case KEY_DOWN:
        case KEY_UP:
            if(*pastChoice == 0 || *pastChoice == 1)
                *pastChoice = 2;
            else
                *pastChoice = 0;
            break;
        case KEY_RIGHT:
            if(*pastChoice == 0 || *pastChoice == 1)
                *pastChoice = (*pastChoice+1)%2;
            break;
        case KEY_LEFT:
            if(*pastChoice == 0 || *pastChoice == 1)
                *pastChoice = *pastChoice-1;
            if(*pastChoice < 0)
                *pastChoice = 1;
            break;
        case 10:
            if(*pastChoice < 2)
                *logGame = *pastChoice;
            else
                *status = SELECT;
            break;
    }
}

static int browseAi(char** p, int* err)
{
    DIR* d = NULL;
    struct dirent *dir;
    d = opendir(aiDirectory);

    char* aiFiles[256];
    int aiNum = 0;

    if(d)
    {
        while((dir = readdir(d)) != NULL && aiNum < 256)
        {
            if(dir->d_type == DT_REG)
            {
                if(strlen(dir->d_name) > 4 && !strcmp(dir->d_name + strlen(dir->d_name) - 3, "-ai"))
                {
                    aiFiles[aiNum] = (char*)calloc(strlen(dir->d_name+2), sizeof(char));
                    strcpy(aiFiles[aiNum++], dir->d_name);
                }
            }
        }
        closedir(d);
    }
    else
    {
        *err = 2;
        return 0;
    }
    
    int choosing = 1;
    int choice = -1;
    int in = 0;
    while(choosing == 1)
    {
        clear();
        noecho();
        curs_set(0);

        mvwprintw(stdscr, 0, 0, "Browsing AIs in %s", aiDirectory);
        int pageSize = LINES-4;
        int maxPage = ((aiNum-1)/pageSize)+1;
        int curPage = (choice >= 0 ? (choice/pageSize) + 1 : 1);
        attron(COLOR_PAIR(6));
        mvwprintw(stdscr, LINES-1, COLS-10, "Page %d/%d", curPage, maxPage);
        attroff(COLOR_PAIR(6));

        attron(COLOR_PAIR(1 + (choice == -1 ? 3 : 0)));
        mvwprintw(stdscr, 2, 4, "Cancel");
        attroff(COLOR_PAIR(1 + (choice == -1 ? 3 : 0)));

        for(int i = 0; i < pageSize && i+(curPage-1)*pageSize < aiNum; i++)
        {
            attron(COLOR_PAIR(3 + (choice == i ? 3 : 0)));
            mvwprintw(stdscr, 3+i, 4, "\u2502%s", aiFiles[i+(curPage-1)*pageSize]);
            attroff(COLOR_PAIR(3 + (choice == i ? 3 : 0)));
        }

        in = wgetch(stdscr);
        switch(in)
        {
            case KEY_UP:
                choice--;
                if(choice < -1)
                    choice = -1;
                break;
            case KEY_DOWN:
                choice++;
                if(choice >= aiNum)
                    choice = aiNum-1;
                break;
            case 10:
                if(choice == -1)
                    choosing = -1;
                else
                {
                    strcpy(*p, aiDirectory);
                    strcat(*p, "/");
                    strcat(*p, aiFiles[choice]);
                    choosing = 0;
                }
                break;
        }
    }

    for(int i = aiNum-1; i >= 0; i--)
        free(aiFiles[i]);
    return choosing+1;
}

static void getPlayer(char** p, int pid)
{
    int isSelecting = 1;
    char in[256] = {0};

    int err = 0;

    while(isSelecting)
    {
        clear();
        mvwprintw(stdscr, LINES/2, COLS/2 - 37/2, "Who's Player %d (human/AI-ai/browse) ?", pid);
        mvwprintw(stdscr, LINES/2 + 2, COLS/2 - 12, "> ");

        attron(COLOR_PAIR(1));
        if(err == 1)
            mvwprintw(stdscr, LINES/2 + 3, COLS/2 - 14, "Invalid AI file name.");
        else if(err == 2)
            mvwprintw(stdscr, LINES/2 + 3, COLS/2 - 14, "Impossible to open current directory.");
        attroff(COLOR_PAIR(1));

        refresh();

        echo();
        curs_set(1);
        mvwscanw(stdscr, LINES/2 + 2, COLS/2 - 10," %250s", in);

        if(!strcmp(in, "browse"))
        {
            if(browseAi(p, &err))
                isSelecting = 0;
            else
                err = 0;
        }
        else if(!strcmp(in, "human"))
        {
            strcpy(*p, "human");
            isSelecting = 0;
        }
        else
        {
#ifndef WINDOWS
            if(access(in, F_OK) != -1) 
            {
                strcpy(*p, in);
                isSelecting = false;
            }
#else
            if(1)
            {
                strcpy(*p, in);
                isSelecting = false;
            }
#endif
            else 
                err = 1;
        }
    }
}

static int handleStart(char** p1, char** p2)
{
    getPlayer(p1, 1);
    strcpy(p1Text, *p1);
    getPlayer(p2, 2);
    strcpy(p2Text, *p2);

    return 1;
}

static void updateMenuCur(ProgramState* state, int* logGame, char** p1, char** p2)
{
    curs_set(0);
    int inMenu = 1;
    int status = SELECT;
    
    int menuChoice = 0;

    while(inMenu)
    {
        switch(status)
        {
            case SELECT:
                handleMenuSelect(&menuChoice, &status);
                break;
            case EXITED:
                inMenu = 0;
                *state = EXITING;
                break;
            case OPTIONS:
                handleOptions(&menuChoice, &status, logGame);
                break;
            case STARTING:
                if(handleStart(p1, p2))
                {
                    *state = GAME_START;
                    inMenu = 0;
                    noecho();
                }
                else
                {
                    status = SELECT;
                }
                break;
            default:
                break;
        }
        
    }

}

static void drawMenuCur(ProgramState state)
{

}

static PlayerAction getGameInputCur(GameState* gState)
{
    char c = 0;

    PlayerAction action = (PlayerAction){PA_OTHER, -1, -1};
    switch (getStatus(gState))
    {
        case PLAYER1:
        case PLAYER2:
        {
            c = (getStatus(gState) == PLAYER1) ? 1 : 2;

            mvwprintw(stdscr, LINES/2 + BOARD_HEIGHT/2 + 1, COLS/2 - BOARD_WIDTH/2 - 4, "It's ");
            attron(COLOR_PAIR(c));
            printw("player %d",c);
            attroff(COLOR_PAIR(c));
            printw(" turn.\n");

            if((getStatus(gState) == PLAYER1 && !strcmp(getPlayerType(gState, 0), "human"))
                || (getStatus(gState) == PLAYER2 && !strcmp(getPlayerType(gState, 1), "human")))           
            {
                curs_set(1);
                int choosing = 1;
                int choice = 0;
                while(choosing)
                {
                    int in;
                    if(getFutureAction(gState).posX != -1)
                    {
                        mvwprintw(stdscr, LINES/2 + BOARD_HEIGHT/2 + 2, COLS/2 - BOARD_WIDTH/2 - 2,"> (C)onfirm");
                        mvwprintw(stdscr, LINES/2 + BOARD_HEIGHT/2 + 3, COLS/2 - BOARD_WIDTH/2 - 2,"> (U)ndo");
                        mvwprintw(stdscr, LINES/2 + BOARD_HEIGHT/2 + 4, COLS/2 - BOARD_WIDTH/2 - 2,"> (Q)uit");
                        if(choice == 0)
                            move(LINES/2 + BOARD_HEIGHT/2 + 2, COLS/2 - BOARD_WIDTH/2 - 2);
                        else if (choice ==1)
                            move(LINES/2 + BOARD_HEIGHT/2 + 3, COLS/2 - BOARD_WIDTH/2 - 2);
                        else
                            move(LINES/2 + BOARD_HEIGHT/2 + 4, COLS/2 - BOARD_WIDTH/2 - 2);
                        in = wgetch(stdscr);
                        switch(in)
                        {
                            case KEY_UP:
                                choice--;
                                if(choice < 0)
                                    choice = 2;
                                break;
                            case KEY_DOWN:
                                choice = (choice + 1) % 3;
                                break;
                            case 10:
                                if(choice == 0)
                                {
                                    case 'C':
                                    case 'c':
                                        action.type = PA_CONFIRM;
                                        choosing = 0;
                                        break;
                                }
                                else if (choice == 1)
                                {
                                    case 'U':
                                    case 'u':
                                        action.type = PA_CANCEL;
                                        choosing = 0;
                                        break;
                                }
                                else
                                {
                                    case 'Q':
                                    case 'q':
                                        action.type = PA_GIVEUP;
                                        choosing = 0;
                                        break; 
                                }
                            default:
                                break;
                        }
                    }
                    else
                    {
                        int tileSelected = 0;
                        mvwprintw(stdscr, LINES/2 + BOARD_HEIGHT/2 + 2, COLS/2 - BOARD_WIDTH/2 - 2, "Pick a tile with the arrow keys and select with Enter");
                        action.type = PA_PLACE;
                        move(LINES/2-BOARD_HEIGHT/2+getAvailableTile(gState, 0).posY*2+2, COLS/2-BOARD_WIDTH/2+getAvailableTile(gState, 0).posX*4+5);
                        do
                        {
                            in = wgetch(stdscr);
                            switch(in)
                            {
                                case KEY_UP:
                                case KEY_LEFT:
                                {
                                    if (tileSelected <= 0)
                                        tileSelected = getAvailableTileSize(gState) - 1;
                                    else
                                        tileSelected--;
                                    break;
                                }
                                case KEY_DOWN:
                                case KEY_RIGHT:
                                {
                                    if (tileSelected >= getAvailableTileSize(gState) - 1)
                                        tileSelected = 0;
                                    else
                                        tileSelected++;
                                    break;
                                }
                                default:
                                    break;
                            }
                            action.x = getAvailableTile(gState, tileSelected).posX;
                            action.y = getAvailableTile(gState, tileSelected).posY;
                            move(LINES/2-BOARD_HEIGHT/2+action.y*2+2, COLS/2-BOARD_WIDTH/2+action.x*4+5);
                        } while (in != 10);
                        choosing = 0;
                        wrefresh(stdscr);
                    }
                }
            }
            else
            {
                mvwprintw(stdscr, LINES/2 + BOARD_HEIGHT/2 + 2, COLS/2 - BOARD_WIDTH/2 - 2, "Press a key to continue...");
                getch();
            }
            return action;
        }
        case GAMEOVER:
        {
            int s1, s2;
            getScore(gState, &s1, &s2);
            move(LINES/2 + BOARD_HEIGHT/2 + 2, COLS/2 - BOARD_WIDTH/2 - 2);
            clrtoeol();
            mvwprintw(stdscr, LINES/2 + BOARD_HEIGHT/2 + 2, COLS/2 - BOARD_WIDTH/2 - 4, "Scores : %d - %d\n", s1, s2);
            mvwprintw(stdscr, LINES/2 + BOARD_HEIGHT/2 + 3, COLS/2 - BOARD_WIDTH/2 - 4, "%s", getGameOverMessage(gState));
            mvwprintw(stdscr, LINES/2 + BOARD_HEIGHT/2 + 5, COLS/2 - BOARD_WIDTH/2 - 2, "Press a key to continue...");
            getch();
            setStatus(gState, CLEANUP);
            break;
        }
        default:
            break;
    }
    return action;
}

static void drawBoard(GameState* gState)
{
    wclear(stdscr);

    mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 - 5, COLS/2 - 4, "OTHELLOX");

    int s1, s2;
    getScore(gState, &s1, &s2);

    attron(COLOR_PAIR(1));
    mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 - 3, COLS/2 - 2 - strlen(getPlayerType(gState,0)), "%s", getPlayerType(gState,0));
    mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 - 2, COLS/2 - 4, "%d", s1);
    attroff(COLOR_PAIR(1));
    mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 - 3, COLS/2 - 1, "vs");
    attron(COLOR_PAIR(2));
    mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 - 3, COLS/2 + 2, "%s", getPlayerType(gState,1));
    mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 - 2, COLS/2 + 2 + (s2 < 10 ? 1 : 0), "%d", s2);
    attroff(COLOR_PAIR(2));

    for (int i = 0; i < 8; i++)
    {
        if(i == 0)
            mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2, COLS/2 - BOARD_WIDTH/2, "Y\\X");
        printw("  %c ",i+1+'0');
    }
    printw("\n");

    // Print top of board
    for (int i = 0; i < 8; i++)
    {
        if (i == 0)
            mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 + 1 + i, COLS/2 - BOARD_WIDTH/2, "   \u250f");
        printw("\u2501\u2501\u2501");
        if (i != 7)
            printw("\u2533");
        else
            printw("\u2513\n");
    }

    // Print grid
    for(int i = 0; i< 8; i++)
    {
        // Print cell content
        for(int j = 0; j < 8; j++)
        {
            char cur = ' ';
            char curColor = 0;
            if(getFutureAction(gState).posX == -1)
            {
                for(int k = getAvailableTileSize(gState)-1; k >= 0; k--)
                    if(getAvailableTile(gState, k).posX == j && getAvailableTile(gState, k).posY == i)
                    {
                        cur = 'X';
                        break;
                    }
            }
            switch(getFutureTile(gState, i, j))
            {
                case T_P1:
                    cur = 'O';
                    curColor = COLOR_RED;
                    break;
                case T_P2:
                    cur = 'O';
                    curColor = COLOR_GREEN;
                    break;
                default:
                    break;
            }
            if(j == 0)
                mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 + 2 + i*2, COLS/2 - BOARD_WIDTH/2, " %d ", i+1);
            printw("\u2503 ");
            attron(COLOR_PAIR(curColor));
            printw("%c ", cur);
            attroff(COLOR_PAIR(curColor));
        }
        printw("\u2503\n");
        
        // Print cell walls
        for(int j = 0; j < 8; j++)
        {
            if(i != 7)
            {
                if(j == 0)
                    mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 + 3 + i*2, COLS/2 - BOARD_WIDTH/2, "   \u2523");
                printw("\u2501\u2501\u2501");
                if(j != 7)
                    printw("\u254b");
                else
                    printw("\u252b\n");
            }
            else
            {
                if(j == 0)
                    mvwprintw(stdscr, LINES/2 - BOARD_HEIGHT/2 + 3 + i*2, COLS/2 - BOARD_WIDTH/2, "   \u2517");
                printw("\u2501\u2501\u2501");
                if(j != 7)
                    printw("\u253b");
                else
                    printw("\u251b\n");
            }
        }
    }
}

static void drawGameCur(GameState* gState)
{
    drawBoard(gState);
}

static void destroyCur()
{
    endwin();
}

#endif
