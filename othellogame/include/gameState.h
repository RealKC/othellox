/**
 * @file gameState.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Fonctions de manipulation de l'état du jeu.
 */

#pragma once

#include <stdio.h>

/**
 * @brief La structure GameState est inaccessible autrement qu'avec les fonctions déclarées ici.
 */
typedef struct sGameState GameState;

/**
 * @brief Énumération décrivant les cases du plateau.
 */
typedef enum
{
    T_EMPTY = -1,
    T_P1 = 0,
    T_P2 = 1
} TileType;

/**
 * @brief Énumération décrivant l'état de l'automate du jeu.
 */
typedef enum
{
    INIT,
    PLAYER1,
    PLAYER2,
    CHECK,
    GAMEOVER,
    CLEANUP
} GameStatus;

/**
 * @brief Structure représentant une action
 */
typedef struct
{
    char posY;
    char posX;
    TileType player;
} GameAction;

/**
 * @brief Structure représentant une position sur le plateau
 */
typedef struct
{
    char posY;
    char posX;
} Vec2_c;


/**
 * @brief Crée une structure GameState
 * Cette structure doit être désallouée avec {@link destroyGameState}
 * @param logGame 1 si le jeu doit enregistrer la partie dans un fichier, 0 sinon
 * @param player1 Nom du joueur 1
 * @param player2 Nom du joueur 2
 * @return GameState* La structure si création réussie, NULL sinon.
 */
GameState* createGameState(int logGame, char* player1, char* player2);

/**
 * @brief Désalloue la structure state
 * @param state Structure GameState à désallouer
 */
void destroyGameState(GameState* state);


/**
 * @brief Récupère l'état du jeu actuel.
 * @param state Structure de jeu
 * @return GameStatus État actuel du jeu
 */
GameStatus getStatus(GameState* state);


/**
 * @brief Récupère la case à la position (posX, posY)
 * @param state Structure de jeu
 * @param posY Position Y
 * @param posX Position X
 * @return TileType Case à la position (posX, posY)
 */
TileType getTile(GameState* state, char posY, char posX);

/**
 * @brief Récupère la case "future" à la position (posX, posY)
 * @param state Structure de jeu
 * @param posY Position Y
 * @param posX Position X
 * @return TileType Case future à la position (posX, posY)
 */
TileType getFutureTile(GameState* state, char posY, char posX);


/**
 * @brief Récupère le plateau de jeu actuel dans board.
 * @param state Structure de jeu
 * @param board Tableau 2D qui sera rempli par la fonction
 */
void getBoard(GameState* state, /*@out@*/ TileType board[8][8]);

/**
 * @brief Récupère le plateau de jeu futur dans board.
 * @param state Structure de jeu
 * @param board Tableau 2D qui sera rempli par la fonction
 */
void getFutureBoard(GameState* state, /*@out@*/ TileType board[8][8]);


/**
 * @brief Récupère le nom du joueur associé à l'identifiant playerNum.
 * @param state Structure de jeu
 * @param playerNum Identifiant du joueur
 * @return char* Nom du joueur
 */
char* getPlayerType(GameState* state, char playerNum);

/**
 * @brief Récupère l'action jouée par le joueur actuel.
 * @param state Structure de jeu
 * @return GameAction Action jouée
 */
GameAction getFutureAction(GameState* state);


/**
 * @brief Récupère le nombre de coups possibles.
 * @param state Structure de jeu
 * @return int Nombre de coups possibles.
 */
int getAvailableTileSize(GameState* state);

/**
 * @brief Récupère le coup possible d'indice index.
 * @param state Structure de jeu
 * @param index Indice du coup à récupérer
 * @return Vec2_c Position du coup
 */
Vec2_c getAvailableTile(GameState* state, int index);


/**
 * @brief Récupère l'accesseur lié à l'IA d'indice index
 * @param state Structure de jeu
 * @param index Indice de l'IA
 * @return struct ai_access* Accesseur lié à l'IA
 */
struct ai_access* getAiHandler(GameState* state, int index);


/**
 * @brief Récupère le fichier d'enregistrement s'il existe
 * @param state Structure de jeu
 * @return FILE* Fichier d'enregistrement s'il existe, NULL sinon.
 */
FILE* getLogFile(GameState* state);


/**
 * @brief Récupère le message de fin de partie
 * @param state Structure de jeu
 * @return char* Message de fin de partie
 */
char* getGameOverMessage(GameState* state);

/**
 * @brief Récupère le score final dans s1 et s2.
 * @param state Structure de jeu
 * @param s1 Pointeur vers un entier qui recevra le score du joueur 1
 * @param s2 Pointeur vers un entier qui recevra le score du joueur 2
 */
void getFinalScore(GameState* state, /*@out@*/ int* s1, /*@out@*/ int* s2);

/**
 * @brief Récupère le joueur ayant gagné la partie
 * @param state Structure de jeu
 */
TileType getWinner(GameState* state);


/**
 * @brief Met l'état du jeu à l'état status.
 * @param state Structure de jeu
 * @param status État du jeu cible
 */
void setStatus(GameState* state, GameStatus status);

/**
 * @brief Met la case à la position (posX, posY) à l'état tileState.
 * @param state Structure de jeu
 * @param posY Position Y de la case
 * @param posX Position X de la case
 * @param tileState État de la case cible
 */
void setTile(GameState* state, char posY, char posX, TileType tileState);

/**
 * @brief Met la case future à la position (posX, posY) à l'état tileState.
 * @param state Structure de jeu
 * @param posY Position Y de la case future
 * @param posX Position X de la case future
 * @param tileState État de la case future cible
 */
void setFutureTile(GameState* state, char posY, char posX, TileType tileState);

/**
 * @brief Met à jour les coups possibles disponibles.
 * @param state Structure de jeu
 * @param availableTile Tableau contenant les coups possibles
 * @param size Taille du tableau availableTile
 */
void setAvailableTile(GameState* state, Vec2_c availableTile[], int size);

/**
 * @brief Sauvegarde le coup actuel du joueur.
 * @param state Structure de jeu
 * @param posY Position Y du coup
 * @param posX Position X du coup
 * @param player Joueur ayant joué le coup
 */
void setFutureAction(GameState* state, char posY, char posX, TileType player);

/**
 * @brief Met le message de fin de partie à message.
 * @param state Structure de jeu
 * @param message Message de fin de partie
 */
void setGameOverMessage(GameState* state, char message[512]);

/**
 * @brief Réinitialise les coups possibles.
 * @param state Structure de jeu
 */
void resetAvailableTiles(GameState* state);

/**
 * @brief Met le gagnant de la partie
 * @param state Structure de jeu
 * @param winner Joueur ayant gagné
 */
void setWinner(GameState* state, TileType winner);
