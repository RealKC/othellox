/**
 * @file logger.h
 * @author Peyrille Benjamin, Rogalle Quentin
 * @brief Fonctions d'enregistrement des parties dans un fichier.
 */

#pragma once

#include <stdio.h>

#include "game.h"

/**
 * @brief Crée un fichier log
 * Le nom du fichier sera "gamelogTIMESTAMP"
 * @return FILE* Fichier log
 */
FILE* createLogFile();

/**
 * @brief Crée un fichier log avec un nom donné
 * 
 * @param name Nom du fichier
 * @param addTimestamp Ajoute l'UNIX timestamp au nom du fichier si différent de 0.
 * @return FILE* Fichier log
 */
FILE* createLogFileName(const char* name, int addTimestamp);


/**
 * @brief Ecrit dans le fichier log le début de partie
 * @param logFile Fichier log
 * @param p1 Nom du joueur 1
 * @param p2 Nom du joueur 2
 */
void logStart(FILE* logFile, char* p1, char* p2);

/**
 * @brief Ecrit dans le fichier log le tour joué
 * @param logFile Fichier log
 * @param action Tour joué
 */
void logTurn(FILE* logFile, GameAction action);

/**
 * @brief Ecrit dans le fichier log le gameover
 * @param logFile Fichier log
 * @param p1 Nom du joueur 1
 * @param score1 Score atteint par le joueur 1
 * @param p2 Nom du joueur 2
 * @param score2 Score atteint par le joueur 2
 */
void logGameOver(FILE* logFile, char* p1, int score1, char* p2, int score2);

/**
 * @brief Ecrit dans le fichier log l'échec d'une IA
 * @param logFile Fichier log
 * @param player Nom de l'IA
 * @param id Identifiant associé à l'IA
 */
void logFailure(FILE* logFile, char* player, char id);

/**
 * @brief Ecrit dans le dépassement de temps d'une IA
 * @param logFile Fichier log
 * @param player Nom de l'IA
 * @param id Identifiant associé à l'IA
 */
void logTimeout(FILE* logFile, char* player, char id);