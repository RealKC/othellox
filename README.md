# Othellox  [![UPVD Logo](https://upload.wikimedia.org/wikipedia/fr/e/e6/UPVD_logo.svg)](https://www.univ-perp.fr)

> Reversi game in C allowing AI-vs-AI game automation

Othellox is a reversi game programmed in C for the 4th Semester of Computer Science in UPVD.

The project contains two folders : `documentation` and `othellogame`.

- `documentation` contains all of the information about the project, the AI API and the reports for the university assignment in French.
- `othellogame` contains the source code and Makefiles used to build the game. All of the commands below, unless specified, are to be run inside of this directory.

## Running

Run the othellox executable.

Othellox has a modular user interface system allowing to change what interface you're using :

- Ncurses interface is the default interface.
- Terminal interface, is used if Ncurses isn't supported. It uses unicode characters and has unintended behaviors on older terminal emulators.
- No-Display interface, is used for shell scripting, or making very fast matches between AIs. It does not support human players.

If running via command-line interface, you can specify multiple options allowing you to choose what interface you'll be using, starting the game directly bypassing the menu.

`Usage : othellox [--help] [-d] [-l] [-t | -n | -c] [--p1 player1-ai] [--p2 player2-ai] [--ai path] [--timeout value]`

- `--help` prints the Usage message
- `-d` starts the game in direct play mode. The game will be closed on game end. It requires both `--p1` and `--p2` arguments to be specified
- `-t` starts the game in Terminal interface; `-n` in No-Display interface; `-c` in Ncurses interface. These options are incompatible with each others.
- `--p1` and `--p2` are the arguments for direct play mode. They must contain the AI file in relative or absolute path.
- `-l` forces Othellox to record a gamelog of the played games.
- `--ai` chooses which path to look for when browsing AIs in the ncurses interface.
- `--timeout` set the timeout time for AI execution. Default: 1s.

### AI difficulties

The AIs are ranked from the worst to the best.

- Basic-AI is the easiest one and places at the first place possible.
- Ordinatrice-AI uses a basic minmax of depth 4.
- Radiation-AI uses alpha-beta pruning of depth 5.
- Scanner-AI uses MTD-f of depth 6.
- XRay-AI uses MTD-f with iterative deepening up to depth 7.
- Oracle-AI uses MTD-f with iterative deepening, considering timeout constraints, with a custom heuristic up to depth 12.

## Dependencies

Othellox requires `ncurses` to be built with ncurses support (see Make options on Building sections)

To be built, Othellox requires `clang`. But it is possible to edit CC variables in the makefiles to point to your favorite compiler.

## Installing

If you're under Arch Linux, Othellox is available on the [AUR](https://aur.archlinux.org/packages/othellox/)

If that is not the case, you will have to build Othellox yourself.

## Building

To build the project run the following :

```shell
make
```

You can add to the following command lines the following options to change the resulting build :

- `DEBUG=yes` : Adds debug symbols to the project and built-in AIs.
- `NO_CURSES=yes` : Builds without ncurses support.

## Tests

Unit tests are ran during compilation. They can also be run independently.
Commands to build the unit tests :

```shell
make othelloxTest
```

To run them :

```shell
./othelloxTest
```

## Making your own AI

To create your own AI, you must add a new folder in `othellogame/ai` named `YOURAI-ai`.

Then, copy the template Makefile and .c from the ai `basic-ai` in the folder `othellogame/ai/basic-ai`.

To build the ai, go to the `othellogame` folder and run `make YOURAI-ai`. Header files will be moved and updated to your AI folder and it will be built according to your AI Makefile.

## Licensing

Othellox is under the MIT Licence, specified in `LICENCE`.

The copyright notice in `LICENCE` must be included in all projects using Othellox as well as binaries of Othellox or projects using Othellox.

## Contact

You can contact us at :
Peyrille Benjamin (peyrille.benjamin@gmail.com)
Rogalle Quentin (quentin.rogalle@etudiant.univ-perp.fr)